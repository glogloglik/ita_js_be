var Q = require('q');
var fs = require('fs');

var readFile = Q.denodeify(fs.readFile);

var readFileLocally = function (file, encoding) {
    var deferred = Q.defer();

    fs.readFile(path, function (err, data) {
            if (err) {
                deferred.reject(err);
            }else {
                deferred.resolve(data);
            }
        })
        return deferred.promise;

    //return deferred.promise;
};

// TODO: read file using denodeify function

var readFile = Q.denodeify(require('fs').readFile);



// TODO: read file using local promise function

var readFilePromise = function(file, encoding){
    return new Promise(function (fulfill, reject){
        fs.readFile(file, encoding, function (err, res){
            if (err) reject(err);
            else fulfill(res);
        });
    });
}


// TODO: read files using both methods in parallel   q.all

[{

},{

}]



